// CRUD Operations

// Insert Documents (CREATE)

/*
	// Inserting only one
	Syntax:
		Inserting one document
			db.collectionName.insertOne({
				"fieldA": "valueA",
				"fieldB": "valueB"
			})
*/
db.users.insertOne({
	"firstName": "Jane",
	"lastName": "Doe",
	"age": 21,
	"email": "janedoe@mail.com",
	"department": "none"
})

// Inserting Many Document
/*
	Syntax:
		db.collectionName.insertMany([
		{
			"fieldA": "valueA",
			"fieldB": "valueB"
		},
		{
			"fieldA": "valueA",
			"fieldB": "valueB"
		}
		])

*/

	db.users.insertMany([
		{
			"firstName": "Stephen",
			"lastName": "Hawking",
			"age": 76,
			"email": "stephenhawking@mail.com",
			"department": "none"
		},
		{
			"firstName": "Neil",
			"lastName": "Armstrong",
			"age": 82,
			"email": "neilarmstrong@mail.com",
			"department": "none"
		}
	])

// Mini Activity: 
/*
	1. Make a new collection with the name "courses"
	2. Insert the following fields and values

		name: Javascript 101,
		price: 5000,
		description: Introduction to JacaScript,
		isActive: true

		name: HTML 101,
		price: 2000,
		description: Introduction to HTML,
		isActive: true

		name: CSS 101,
		price: 2500,
		description: Introduction to CSS,
		isActive: false
*/

db.courses.insertMany([
		{
			"name": "Javascript 101",
			"price": 5000,
			"description": "Introduction to JacaScript",
			"isActive": true
		},
		{
			"name": "HTML 101",
			"price": 2000,
			"description": "Introduction to HTML",
			"isActive": true
		},
		{
			"name": "CSS 101",
			"price": 2500,
			"description": "Introduction to CSS",
			"isActive": false
		}
	])

// Find Documents (READ)

/*
	Syntax:
		db.collectionName.find() - this will retrieve all our documents
		db.collectionName.find({"criteria": "value"}) - this will retrieve all our documents that will match with our criteria

		db.collectionName.findOne({""}) - this will return the first document in our collection
		db.collectionName.findOne({"criteria": "value"}) - tis will return the first document in our collection that will match our criteria

*/

db.users.find();

db.users.find({"firstName": "Jane"});

db.users.findOne();

db.users.findOne({"department": "none"});

// Updating Documents (Update)
// Updating one
/*
	Syntax:
		db.collectionName.updateOne({
			"cirterian": "value"
		},
		{
			$set: {
				"fieldToBeUpdated": "updatedValue"
			}
		}
		);
*/

db.users.insertOne( {
		"firstName": "Test",
		"lastName": "Test",
		"age": 0,
		"email": "Test@mail.com",
		"department": "none"
	});

// Updating One Document
db.users.updateOne(
		{
			"firstName": "Test"
		},
		{
			$set: {
				"firstName": "Bill",
				"lastName": "Gates",
				"age": 65,
				"email": "billgates@mail.com",
				"department": "Operations",
				"status": "active"
			}
		}
	)

// Updating Many/Multiple Documents
/*
	Syntax:
		db.collectionName.updateMany({
			"criterian": "value"
		},
		{
			$set: {
				"fieldToBeUpdated": "updatedValue"
			}
		}
		);
*/
db.users.updateMany(
	{
		"department": "none"
	},
	{
		$set: {
			"department": "HR"
		}
	}
);

db.users.updateOne(
		{
			"firstName": "Jane",
			"lastName": "Doe"
		},
		{
			$set: {
				"dept": "Operations"
			}
		}

	);

db.users.updateOne({
		"firstName": "Bill",
		},
		{
			$unset: {
				"ststus": "active"
			}
		}
	);
// renaming
db.users.updateMany(
	{},
	{
		$rename: {
			"department": "dept"
		}
	}
);

/*
	Mini Activity:
	1. In our courses collection, update the HTML 101 course
		- Make the isActive to false
	2. Add enrollees field to all the documents in our courses collection
		- Enrollees: 10
*/

db.courses.updateOne (
	{
		"name": "HTML 101"
	},
	{
		$set: {
			"isActive": false
		}
	}
);

db.courses.updateMany (
		{},
		{
			$set: {
				"enrollees": 10
			}
		}
	);

// Deleting Documents (DELETE)

/*
	Syntax:
		db.collectionName.deleteOne({"criteria": "value"}) - only one will be deleted
		db.collectionName.deleteMany({}) - many
*/

db.users.insertOne({
	"firstName": "Test"
});
// Deleting One
db.users.deleteOne({"firstName": "Test"});
// Deleting Many
db.users.deleteMany({"dept": "HR"}); 
// Deleting ALL
db.courses.deleteMany({});